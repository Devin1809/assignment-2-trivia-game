import vue from 'vue'
import vuex from 'vuex'

vue.use(vuex);
export default new vuex.Store({
    state: {
        questionAmount : "",
        categoryChosen : "",
        difficultyChosen : "",
        typeChosen : "",
        results : "",
        chosenAnswers : [],
        actualScore: 0,
        maxScore: 0

    },
    mutations: {
        setQuestionAmount: (state, payload) => {
            state.questionAmount = payload;
        },
        setCategory: (state, payload) => {
            state.categoryChosen = payload;
        },
        setDifficulty: (state, payload) => {
            state.difficultyChosen = payload;
        },
        setType: (state, payload) => {
            state.typeChosen = payload;
        },
        setCurrentQuestion: (state, payload) => {
            state.setCurrentQuestion = payload;
        },
        setResults(state, payload) {
            state.results = payload;
        },
        setSelectedAnswer(state, payload) {
            state.chosenAnswers = [...state.chosenAnswers, payload];
        },
        setCurrentScore(state, payload) {
            state.currentScore = payload;
        },
        setMaxScore(state, payload) {
            state.maxScore = payload;
        },
        resetSelectedAnswer(state) {
            state.chosenAnswers = "";
        }
    },
    getters: {

    },
    actions: {
        async getQuiz({ state, commit }) {
            let amount = `amount=${state.questionAmount}`;
            let category;
            let difficulty;
            let type;
            let APIURL;

            if(category != "") {
                category = `&category=${state.categoryChosen}`
            }
            if(difficulty != "") {
                difficulty = `&difficulty=${state.difficultyChosen}`
            }
            if(type != "") {
                type = `&type=${state.typeChosen}`
            }

            APIURL = `https://opentdb.com/api.php?${amount}${category}${difficulty}${type}`;
            

            const response = await fetch(APIURL)
            const { results } = await response.json()
            
            commit('setResults', results)
        }         
    }
    
})